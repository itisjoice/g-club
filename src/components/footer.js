import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';




const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        boxShadow: '0 2px 4px 0 rgba(0,0,0,.03)'

    },
    font:{
        color:'black',
        padding: theme.spacing(3)
    }





}));

export default function Footer() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.footer}> 
            <Container>
                <Toolbar>
                <Grid container direction="row" justify="center" alignItems="center" >
                <p className={classes.font}>Copyright © 2020 gclubauto168.com All rights reserved.</p>
                </Grid>

                </Toolbar>
            </Container>
            </AppBar>
        </div >

    );
}
