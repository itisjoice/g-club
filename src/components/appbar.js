import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Image from "../components/image"
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import logo from '../images/Gclubauto168.png'; // Tell webpack this JS file uses this image



const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    appbar: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        boxShadow: '0 2px 4px 0 rgba(0,0,0,.03)'


    },

    menuButton: {
        display: 'flex', '& > *': {
            margin: theme.spacing(),
        },
    },
    title: {
        flexGrow: 1,
        color: '#2f2f2f',
        display:'flex',
        // alignItems:'center',
        // justifyContent:'flex-end',
        flexDirection: 'row',
        // padding:'12px'
  
    },
    title2: {
        flexGrow: 1,
        color: '#2f2f2f',
        display:'flex',
        // alignItems:'center',
        justifyContent:'flex-end',
        flexDirection: 'row',
        // padding:'12px'
  
    },
    
    textfield: {

        margin: theme.spacing(1),
        height: '0.2 em'
    },
    logo: {
        
        width: '6%',
        height: '6%'
        

    },




}));

export default function AppbarGClub() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.appbar}> <Container>
                <Toolbar>
                    {/* <Typography className={classes.title}> */}
                    
                        <img src={logo} alt="Logo" className={classes.logo} />

                        {/* </Typography> */}
                        <div className={classes.title} >
                        <div className={classes.menuButton}>
                        <Button size="small" variant="outlined" color="secondary" href="/"><b2>หน้าแรก</b2>
                             
                      </Button>
                        <Button to="/Register/" size="small" variant="outlined" color="secondary" href="/Promotion/"><b2>โปรโมชั่น</b2>
                            
                      </Button>
                      <Button to="/Register/" size="small" variant="outlined" color="secondary" href="/contact/"><b2>ติดต่อเรา</b2>
                            
                      </Button>
                      </div>
                    </div>


                    <div className={classes.title2}>
                        <TextField className={classes.textfield} id="outlined-basic" label="ชื่อผู้ใช้" variant="outlined" />
                        <TextField className={classes.textfield} id="outlined-basic" label="รหัสผ่าน" variant="outlined" />
                        <div className={classes.menuButton}>
                        <Button size="small" variant="contained" color="secondary"><b2>login</b2>
                            
                      </Button>
                        
                        <Button size="small" variant="contained" color="primary" href="/Register/" ><b2>สมัครสมาชิก</b2>
                            
                      </Button>
                      </div>
                    </div>

                </Toolbar>
            </Container>
            </AppBar>
        </div >

    );
}
