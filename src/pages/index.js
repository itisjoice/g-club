import React from "react"
import Container from '@material-ui/core/Container';
import { Link } from "gatsby"

import Image from "../components/image"
import AppbarGClub from "../components/appbar"
import Image2 from '../images/Jackpot168-01.png'
import Image3 from '../images/Promotion-slot-02.png'
import { makeStyles } from '@material-ui/core/styles';
import Register from './Register'
import CountUp from 'react-countup';
import { Grid, Typography } from "@material-ui/core";
import Footer from "../components/footer";



const useStyles = makeStyles((theme) => ({



  jackpot: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {

      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'

    },

    display: 'flex',


  },
  wpblock2: {
    // width: 'auto',
    // height: 'auto'
    // float: 'right'


  },
  wpblock3: {
    // width: '50%',
    // float: 'left'


  },
  bonus: {
    // marginLeft: '5rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

  },
  bigwinimg: {
    flexGrow: 1,
    background: 'red'
  }




}));

const IndexPage = () => {
  const classes = useStyles()
  return (
    <div>
      <AppbarGClub />
      <Container >
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6} className={classes.bonus}>
            <Typography variant="h4" gutterBottom><h1>JACKPOT ที่กำลังออกตอนนี้</h1></Typography>
            <hb><CountUp end={10000000} /></hb>

          </Grid>
          <Grid item xs={12} sm={6}>
            {/* <div className={classes.bigwinimg}> */}
            <img src={Image2} className={classes.wpblock2} />
            {/* </div> */}
          </Grid>

          <div><center>
            <h1>G-club 168  สมัครวันนี้ พร้อมรับโบนัส ฟรี 50% ทันที</h1>
            <h4>G-club 168 อย่างเป็นทางการเว็บตรงบริษัทแม่ ให้บริการแล้ว รองรับภาษาไทย 100% สนุกได้มากกว่า 60 เกมส์</h4>
            <p>ถึงเวลาของความสนุกแล้ว กับเว็บไซต์สล็อตออนไลน์อย่างเป็นทางการส่งตรงจากประเทศอังกฤษ เกมส์รูปแบบใหม่ ที่จะพาทุกคนสนุกในรูปแบบของเกมส์บนมือถือ
            ที่เล่นได้จากทุกที่ทุกเวลา พร้อมระบบบริการเต็มรูปแบบ สมัคร ฝากและถอน ผ่านระบบอัตโนมัติ เติมเร็ว 3 วินาที ภายในพริบตา ครั้งแรกที่การเล่นสล็อตจะเปลี่ยนไปตลอดกาล
           กับรูปแบบที่แปลกใหม่ น่าเล่นมากขึ้น สนุกมากขึ้น โบนัสใหญ่ขึ้น</p>
          </center>
          </div>


          <Grid item xs={12} sm={6} className={classes.bonus}>
            <img src={Image3} className={classes.wpblock3} />
          </Grid>
          <Grid item xs={12} sm={6} className={classes.bonus}>
            <h2>Pg Slot เล่นสนุกทุกระบบ ไม่ต้องดาวน์โหลด เล่นได้ผ่านเว็บไซต์</h2>
            <p>สล็อตออนไลน์เล่นได้แล้ววันนี้ รองการเล่นสล็อตออนไลน์ได้ทั้งคอมพิวเตอร์ แท็บเลตและมือถือสมาร์ทโฟน รองรับทั้งระบบ iOS และ Android ผ่านได้ผ่านเว็บบราวเซอร์
            Google Chrome และ Safari ใช้งานได้กับระบบปฏิบัติการ Windows, Mac OS ของค่าย Apple และรองรับ HTML5 เล่นได้ผ่านเว็บไซต์บนมือถือ
            โดยไม่ต้องดาวน์โหลดและติดตั้ง App สะดวกสบาย ครบครัน รองรับการใช้งานภาษาไทย ตอบโจทย์การเล่นสล็อตออนไลน์ให้ชาวไทยโดยเฉพาะ
            พร้อมยังมีเกมส์อีกมากมาย เช่น เกมโป๊กเกอร์ เกมบอร์ด เกมยิงปลา มีระบบเกมส์ที่ทันสมัย สมาชิกสามารถเพลิดเพลินกับการเล่นเกมส์ที่เข้าใจง่าย สะดวกสบาย
            ที่สำคัญระบบมีปลอดภัยที่สุด เราพร้อมเสิร์ฟความบันเทิงและความรวยทุกเวลาทุกสถานที่</p>
          </Grid>

          <div >


            <h1>PGSLOT สล็อต รูปแบบใหม่</h1>
            <p>สล็อตออนไลน์รูปแบบ 3D มิติใหม่ของเกมส์สล็อต เปิดประสบการณ์การเล่นเกมส์สล็อตที่เหนือชั้น
            ทันสมัย มีเนื้อเรื่องที่ดี เสียงเอฟเฟต์ที่น่าตื่นเต้น และมีภาพเคลื่อนไว้ที่เสมือนจริงที่สุด ซึ่งในแต่ละเกมก็จะมีความแตกต่างกันออกไป
            มีเอกลักษณ์เป็นของตัวเองทำให้ผู้เล่นมีความสนุก เพลิดเพลินไปกับเกมจนแทบไม่อยากจะหยุดเล่น เหนือขอบเขตของความเสมือนจริงด้วยนวัตกรรม
            และงานศิลปะผสมผสานกับจินตนาการของเรื่องราวต่างๆ ถ่ายทอดออกมาเป็นฉากที่สวยงาม สร้างความตื่นเต้นอยู่ตลอดเวลาและที่สำคัญเป็นเกมที่แจ็คพอตออกเยอะมาก
             สมัครสมาชิกได้แล้ววันนี้ รับโบนัสทันที 50% </p>
          </div>
        </Grid>
        
      </Container>
      <Footer/>
    </div>

  )
}





export default IndexPage
