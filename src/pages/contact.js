import React from "react"
import Container from '@material-ui/core/Container';
import { Link } from "gatsby"

import Image from "../components/image"
import AppbarGClub from "../components/appbar"
import line from '../images/linegclubde-08.png'
import Image3 from '../images/Promotion-slot-02.png'
import { makeStyles } from '@material-ui/core/styles';
import Register from './Register'
import CountUp from 'react-countup';
import { Grid, Typography, Divider } from "@material-ui/core";
import Footer from "../components/footer";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';


const useStyles = makeStyles((theme) => ({



  jackpot: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {

      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'

    },

    display: 'flex',


  },

  Rfont : {
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent:'center',
    alignItems:'center'
  }
  







}));

const IndexPage = () => {
  const classes = useStyles()
  return (
    <div>
      <AppbarGClub />
      <Container ><div className={classes.Rfont}> <h1>ช่องทางการติดต่อ</h1></div>
      <Divider/>
      <div className={classes.Rfont}> <h1>LINE: @gclubde</h1></div>
      <img src={line}/>

      </Container>
      <Footer/>
    </div>

  )
}





export default IndexPage
