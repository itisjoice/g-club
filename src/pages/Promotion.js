import React from "react"
import Container from '@material-ui/core/Container';
import { Link } from "gatsby"
import AppbarGClub from "../components/appbar"
import Imagest from '../images/Promotion-slot-01.png'
import Image1 from '../images/Promotion-slot-03.png'
import Image2 from '../images/Promotion-slot-04.png'
import Image3 from '../images/Promotion-slot-05.png'
import { makeStyles } from '@material-ui/core/styles';
import Footer from "../components/footer";


const useStyles = makeStyles((theme) => ({



  jackpot: {
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'

    },

    display: 'flex',


  },
  wpblock2: {
    width: '50%',
    float: 'left',
    display: 'flex',
    flexGrow: 1


  },
  wpblock3: {
    width: '35%',
    padding: theme.spacing(3),




  },
  content: {
    flexGrow: 1,
    padding: '80px',
    marginright: '5rem',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'

  },
  ImageCd: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'


  }




}));

const IndexPage = () => {
  const classes = useStyles()
  return (
    <div>
      <AppbarGClub />
      <Container>

        <div>

          <img src={Imagest} className={classes.wpblock2} />

        </div>
        <div className={classes.content}>
        <h1>โปรโมชั่นสุดพิเศษ</h1>
          <p>ทาง PG SLOT ได้ต้อนรับสมาชิกใหม่ด้วยโปรโมชั่นที่ร้อนแรง หากท่าน สมัครสมาชิก PGSLOT วันนี้ ฝากครั้งแรกรับเครดิตฟรีไปเลย สูงสุดที่ 500 บาท เพียงสมัครสมาชิกครั้งแรกขั้นต่ำ 100 บาท ก็สามารถรับสิทธิ์ได้เลย
          
แต่ยังไม่หมดเพียงเท่านี้ ยังมีโปรโมชั่นพิเศษมากมายรอท่านอยู่ สามารถตรวจสอบ โปรโมชั่น PG SLOT ใหม่ๆ ที่อัพเดตได้ก่อนใคร พร้อมหรือยังที่เปลี่ยนเงินหลักร้อยเป็นหลักแสน พร้อมประสบการณ์เล่นสล็อตรูปแบบใหม่ที่ไม่เหมือนใคร ภาพสวยคมชัด สามารถเล่นผ่านมือถือได้ง่ายๆ รองรับทุกระบบทั้ง IOS Android</p></div>

        <div className={classes.ImageCd}>
          <img src={Image1} className={classes.wpblock3} />
          <img src={Image2} className={classes.wpblock3} />
          <img src={Image3} className={classes.wpblock3} />
        </div>

      </Container>
      <Footer/>
    </div>

  )
}





export default IndexPage
