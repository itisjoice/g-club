import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import logo from '../images/Gclubauto168.png';
import AppbarGClub from "../components/appbar"
import Button from '@material-ui/core/Button';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';
import { Divider } from '@material-ui/core';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

const currencies = [
  {
    value: 'start',
    label: '--เลือกธนคารของท่าน--',
  },
  {
    value: 'SCB',
    label: 'ธนคารไทยพาณิช',
  },
  {
    value: 'KBANK',
    label: 'ธนคารกสิกรไทย',
  },
  {
    value: 'BBL',
    label: 'ธนคารกรุงเทพ',
  },

  {
    value: 'JPY',
    label: 'ธนคารกรุงไทย',
  },

];

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2)
    
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  textfield: {
    margin: theme.spacing(1),
    height: '0.2 em',
  },
  textfield2: {
    marginTop:'1rem',
    marginBottom:'1rem',
    margin: '0px 0px 0px',
    height: '0.1 em',

  },
  

  logo: {

    width: '20%',
    height: '20%'


  },


  LoginBox: {
    flexGrow: 1,
    color: '#2f2f2f',
    display: 'flex',
    justifyContent: 'flex-end',
    flexDirection: 'column',


  

  },
  divlogo:{
    justifyContent:'center'
  }
}));

export default function MultilineTextFields() {
  const classes = useStyles();
  const [currency, setCurrency] = React.useState('EUR');
  const [value, setValue] = React.useState('yes');



  const handleChange = (event) => {
    setCurrency(event.target.value);
    setValue(event.target.value);
  };

  return (
    
<div>
<AppbarGClub/>
    <Container>
      

      <div className={classes.root}>
        <Grid container direction="row" justify="center" alignItems="center">

          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <div className={classes.divlogo}>
                <img src={logo} alt="Logo" className={classes.logo} />

              </div>
             <h1>สมัครสมาชิก</h1>
              <Divider className={classes.textfield}/>
              
      
              
  
              <div style={{ display: 'flex', flexDirection: 'column' }}>
              
              <TextField className={classes.textfield} id="outlined-basic" label="เบอร์โทร" variant="outlined" />
                <TextField className={classes.textfield} id="outlined-basic" label="ชื่อจริง" variant="outlined" helperText="*ไม่ต้องมีคำนำหน้าชื่อ" />
                <TextField className={classes.textfield} id="outlined-basic" label="นามสกุล" variant="outlined" />
                <TextField
                  className={classes.textfield}
                  id="outlined-select-currency-native" 
                  select
                  label="ธนาคาร"
                  value={currency}
                  onChange={handleChange}           
                  SelectProps={{
                    native: true,
                  }}
                  variant="outlined"
                >
                  {currencies.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}

                </TextField>
                <TextField className={classes.textfield} id="outlined-basic" label="เลขบัญชี" variant="outlined" />
                <TextField className={classes.textfield} id="outlined-basic" label="รหัสผ่าน" variant="outlined" helperText="*ต้องเป็นตัวอักษรพิมพ์ใหญ่และพิมเล็ก(A-Z)และตัวเลข(0-9) ไม่ต่ำกว่า 8 ตัวอักษร" />


              </div><br/>
              <div>
              <FormControl component="fieldset">
      <FormLabel component="legend"><h3>สมาชิกใหม่เลือกรับโนัสและโปรโมชั่น</h3></FormLabel>
      <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange} >
        <FormControlLabel value="yes" control={<Radio />} label="รับโบนัสและโปรโมชั่น" />
        <FormControlLabel value="no" control={<Radio />} label="ไม่รับโบนัสและโปรโมชั่น" /><br/>
       
      </RadioGroup>
    </FormControl>
    </div>
              <Button size="large" variant="contained" color="secondary">ถัดไป</Button>
              <p><span>คุณเป็นสมาชิกอยู่แล้ว?</span><Button><p>เข้าสู่ระบบ</p></Button></p>
              
             
     
              <Divider className={classes.textfield}/>

              <div className={classes.textfield}>
              <h3>------ โปรดอ่าน ------</h3>
                <p> 1. ชื่อ - นามสกุล ที่ใช้ในการสมัครสมาชิกจะต้องตรงกับข้อมูลบัญชีธนาคาร เท่านั้น<br/>
2. ต้องใช้บัญชีธนาคารที่สมัครเท่านั้น ในการฝาก - ถอนเงิน ในกรณีใช้บัญชีอื่นฝากเข้ามา ทางเราขอสงวนสิทธิ์การทำรายการทุกกรณี<br/>
3. สมาชิก 1 คน ต่อ 1 ไอดี เท่านั้น กรณีที่เราตรวจพบว่ามีการสมัครหลายยูส ทางเราขอสงวนสิทธิ์ในการถอนเงินและระงับบัญชีทุกกรณี<br/>
4. หากเกิดข้อผิดพลาดของระบบให้แจ้งพนักงานทันที กรณีที่ไม่แจ้งทางเราขอสงวนสิทธิ์การถอนเงินทุกกรณี</p></div>
              
              
              {/* </div> */}
              {/* </form> */}
            </Paper>
          </Grid>
        </Grid>
      </div>
    </Container>
    </div>
  );
}
