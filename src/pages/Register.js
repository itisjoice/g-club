import React from "react"
import Container from '@material-ui/core/Container';
import { Link } from "gatsby"
import AppbarGClub from "../components/appbar"
import Button from '@material-ui/core/Button';
import Imagest from '../images/Promotion-slot-01.png'
import Image1 from '../images/Promotion-slot-03.png'
import Image2 from '../images/Promotion-slot-04.png'
import Image3 from '../images/Promotion-slot-05.png'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import logo from '../images/Gclubauto168.png';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';




const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2)

  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  textfield: {

    margin: theme.spacing(1),
    height: '0.2 em',



  },
  logo: {

    width: '20%',
    height: '20%'


  },


  LoginBox: {
    flexGrow: 1,
    color: '#2f2f2f',
    display: 'flex',
    justifyContent: 'flex-end',
    flexDirection: 'column',


  },
  

}));

export default function AutoGrid() {
  const classes = useStyles();

  return (
    <div>
<AppbarGClub />
    <Container>
      
      <div className={classes.root}>
      <Grid container direction="row" justify="center" alignItems="center">

          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <img src={logo} alt="Logo" className={classes.logo} />
              <div className={classes.LoginBox}>
                <TextField className={classes.textfield} id="outlined-basic" label="เบอร์โทร" variant="outlined" />
                {/* <TextField className={classes.textfield} id="outlined-basic" label="เลขบัญชี" variant="outlined" /> */}
             
                <Button size="large" variant="contained" color="secondary" href="/Register2/" ><b2>ถัดไป</b2></Button>
                <p><span>คุณเป็นสมาชิกอยู่แล้ว?</span><Button href="/loginpage/"><p>เข้าสู่ระบบ</p></Button></p>
                {/* <Button size="large" variant="contained" color="primary">สมัครสมาชิก</Button> */}
                
                </div>

            </Paper>
          </Grid>

        </Grid>
      
      </div>
    </Container>
    </div>
    
  );
}